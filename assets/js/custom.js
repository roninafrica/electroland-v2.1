//////////////////
// Navigation
//////////////////
jQuery(document).ready(function($) {
    var mainHeader = $(".cd-auto-hide-header"),
        secondaryNavigation = $(".cd-secondary-nav"),
        //this applies only if secondary nav is below intro section
        belowNavHeroContent = $(".sub-nav-hero"),
        headerHeight = mainHeader.height();

    //set scrolling variables
    var scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 150;

    mainHeader.on("click", ".nav-trigger", function(event) {
        // open primary navigation on mobile
        event.preventDefault();
        mainHeader.toggleClass("nav-open");
    });

    $(window).on("scroll", function() {
        if (!scrolling) {
            scrolling = true;
            !window.requestAnimationFrame ?
                setTimeout(autoHideHeader, 250) :
                requestAnimationFrame(autoHideHeader);
        }
    });

    $(window).on("resize", function() {
        headerHeight = mainHeader.height();
    });

    function autoHideHeader() {
        var currentTop = $(window).scrollTop();

        belowNavHeroContent.length > 0 ?
            checkStickyNavigation(currentTop) // secondary navigation below intro
            :
            checkSimpleNavigation(currentTop);

        previousTop = currentTop;
        scrolling = false;
    }

    function checkSimpleNavigation(currentTop) {
        //there's no secondary nav or secondary nav is below primary nav
        if (previousTop - currentTop > scrollDelta) {
            //if scrolling up...
            mainHeader.removeClass("is-hidden");
        } else if (
            currentTop - previousTop > scrollDelta &&
            currentTop > scrollOffset
        ) {
            //if scrolling down...
            mainHeader.addClass("is-hidden");
        }
    }

    function checkStickyNavigation(currentTop) {
        //secondary nav below intro section - sticky secondary nav
        var secondaryNavOffsetTop =
            belowNavHeroContent.offset().top -
            secondaryNavigation.height() -
            mainHeader.height();

        if (previousTop >= currentTop) {
            //if scrolling up...
            if (currentTop < secondaryNavOffsetTop) {
                //secondary nav is not fixed
                mainHeader.removeClass("is-hidden");
                secondaryNavigation.removeClass("fixed slide-up");
                belowNavHeroContent.removeClass("secondary-nav-fixed");
            } else if (previousTop - currentTop > scrollDelta) {
                //secondary nav is fixed
                mainHeader.removeClass("is-hidden");
                secondaryNavigation.removeClass("slide-up").addClass("fixed");
                belowNavHeroContent.addClass("secondary-nav-fixed");
            }
        } else {
            //if scrolling down...
            if (currentTop > secondaryNavOffsetTop + scrollOffset) {
                //hide primary nav
                mainHeader.addClass("is-hidden");
                secondaryNavigation.addClass("fixed slide-up");
                belowNavHeroContent.addClass("secondary-nav-fixed");
            } else if (currentTop > secondaryNavOffsetTop) {
                //once the secondary nav is fixed, do not hide primary nav if you haven't scrolled more than scrollOffset
                mainHeader.removeClass("is-hidden");
                secondaryNavigation.addClass("fixed").removeClass("slide-up");
                belowNavHeroContent.addClass("secondary-nav-fixed");
            }
        }
    }

    // TimeOut Brands
    setTimeout(function() {
        $("#brandsNav").collapse("hide");
    }, 5000);

    //////////////////
    // Cart Page
    //////////////////
    /* Set rates + misc */
    var taxRate = 0.05;
    var shippingRate = 15.0;
    var fadeTime = 300;

    /* Assign actions */
    $(".product-quantity input").change(function() {
        updateQuantity(this);
    });

    $(".product-removal button").click(function() {
        removeItem(this);
    });

    /* Recalculate cart */
    function recalculateCart() {
        var subtotal = 0;

        /* Sum up row totals */
        $(".product").each(function() {
            subtotal += parseFloat($(this).children(".product-line-price").text());
        });

        /* Calculate totals */
        var tax = subtotal * taxRate;
        var shipping = subtotal > 0 ? shippingRate : 0;
        var total = subtotal + tax + shipping;

        /* Update totals display */
        $(".totals-value").fadeOut(fadeTime, function() {
            $("#cart-subtotal").html(subtotal.toFixed(2));
            $("#cart-tax").html(tax.toFixed(2));
            $("#cart-shipping").html(shipping.toFixed(2));
            $("#cart-total").html(total.toFixed(2));
            if (total == 0) {
                $(".checkout").fadeOut(fadeTime);
            } else {
                $(".checkout").fadeIn(fadeTime);
            }
            $(".totals-value").fadeIn(fadeTime);
        });
    }

    /* Update quantity */
    function updateQuantity(quantityInput) {
        /* Calculate line price */
        var productRow = $(quantityInput).parent().parent();
        var price = parseFloat(productRow.children(".product-price").text());
        var quantity = $(quantityInput).val();
        var linePrice = price * quantity;

        /* Update line price display and recalc cart totals */
        productRow.children(".product-line-price").each(function() {
            $(this).fadeOut(fadeTime, function() {
                $(this).text(linePrice.toFixed(2));
                recalculateCart();
                $(this).fadeIn(fadeTime);
            });
        });
    }

    /* Remove item from cart */
    function removeItem(removeButton) {
        /* Remove row from DOM and recalc cart total */
        var productRow = $(removeButton).parent().parent();
        productRow.slideUp(fadeTime, function() {
            productRow.remove();
            recalculateCart();
        });
    }
});

(function($) {

    $('#search-button').on('click', function(e) {
        if ($('#search-input-container').hasClass('hdn')) {
            e.preventDefault();
            $('#search-input-container').removeClass('hdn')
            return false;
        }
    });

    $('#hide-search-input-container').on('click', function(e) {
        e.preventDefault();
        $('#search-input-container').addClass('hdn')
        return false;
    });

})(jQuery);

// Smooth Scroll Config
// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href="#featured"]')
    .not('[href="#all_specs"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $("html, body").animate({
                        scrollTop: target.offset().top,
                    },
                    500,
                    function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) {
                            // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        }
                    }
                );
            }
        }
    });

//////////////////////
//// Toggle text change
/////////////////////////
// $('[data-toggle="collapse"]').click(function() {
//   $(this).toggleClass('active');
//   if ($(this).hasClass('active')) {
//     $(this).text('Hide All Specs');
//   } else {
//     $(this).text('Show All Specs');
//   }
// });

///////////////////////
/// Find all YouTube videos
//////////////////////

var $allVideos = $(
        "iframe[src^='//player.vimeo.com'], iframe[src^='//www.youtube.com']"
    ),
    // The element that is fluid width
    $fluidEl = $("body");

// Figure out and save aspect ratio for each video
$allVideos.each(function() {
    $(this)
        .data("aspectRatio", this.height / this.width)

    // and remove the hard coded width/height
    .removeAttr("height")
        .removeAttr("width");
});

// When the window is resized
$(window)
    .resize(function() {
        var newWidth = $fluidEl.width();

        // Resize all videos according to their own aspect ratio
        $allVideos.each(function() {
            var $el = $(this);
            $el.width(newWidth).height(newWidth * $el.data("aspectRatio"));
        });

        // Kick off one resize to fix all videos on page load
    })
    .resize();

// Image Hover Switch
var sourceSwap = function() {
    var $this = $(this);
    var newSource = $this.data("alt-src");
    $this.data("alt-src", $this.attr("src"));
    $this.attr("src", newSource);
};

$(function() {
    $("img.img_switch").hover(sourceSwap, sourceSwap);
});

//////////////////
// Product Single
//////////////////
var galleryItems = $(".cd-gallery").children("li");

galleryItems.each(function() {
    var container = $(this),
        // create slider dots
        sliderDots = createSliderDots(container);
    //check if item is on sale
    updatePrice(container, 0);

    // update slider when user clicks one of the dots
    sliderDots.on("click", function() {
        var selectedDot = $(this);
        if (!selectedDot.hasClass("selected")) {
            var selectedPosition = selectedDot.index(),
                activePosition = container.find(".cd-item-wrapper .selected").index();
            if (activePosition < selectedPosition) {
                nextSlide(container, sliderDots, selectedPosition);
            } else {
                prevSlide(container, sliderDots, selectedPosition);
            }

            updatePrice(container, selectedPosition);
        }
    });

    // update slider on swipeleft
    container.find(".cd-item-wrapper").on("swipeleft", function() {
        var wrapper = $(this);
        if (!wrapper.find(".selected").is(":last-child")) {
            var selectedPosition =
                container.find(".cd-item-wrapper .selected").index() + 1;
            nextSlide(container, sliderDots);
            updatePrice(container, selectedPosition);
        }
    });

    // update slider on swiperight
    container.find(".cd-item-wrapper").on("swiperight", function() {
        var wrapper = $(this);
        if (!wrapper.find(".selected").is(":first-child")) {
            var selectedPosition =
                container.find(".cd-item-wrapper .selected").index() - 1;
            prevSlide(container, sliderDots);
            updatePrice(container, selectedPosition);
        }
    });

    // preview image hover effect - desktop only
    container.on("mouseover", ".move-right, .move-left", function(event) {
        hoverItem($(this), true);
    });
    container.on("mouseleave", ".move-right, .move-left", function(event) {
        hoverItem($(this), false);
    });

    // update slider when user clicks on the preview images
    container.on("click", ".move-right, .move-left", function(event) {
        event.preventDefault();
        if ($(this).hasClass("move-right")) {
            var selectedPosition =
                container.find(".cd-item-wrapper .selected").index() + 1;
            nextSlide(container, sliderDots);
        } else {
            var selectedPosition =
                container.find(".cd-item-wrapper .selected").index() - 1;
            prevSlide(container, sliderDots);
        }
        updatePrice(container, selectedPosition);
    });
});

function createSliderDots(container) {
    var dotsWrapper = $('<ol class="cd-dots"></ol>').insertAfter(
        container.children("a")
    );
    container.find(".cd-item-wrapper li").each(function(index) {
        var dotWrapper =
            index == 0 ? $('<li class="selected"></li>') : $("<li></li>"),
            dot = $('<a href="#0"></a>').appendTo(dotWrapper);
        dotWrapper.appendTo(dotsWrapper);
        dot.text(index + 1);
    });
    return dotsWrapper.children("li");
}

function hoverItem(item, bool) {
    item.hasClass("move-right") ?
        item
        .toggleClass("hover", bool)
        .siblings(".selected, .move-left")
        .toggleClass("focus-on-right", bool) :
        item
        .toggleClass("hover", bool)
        .siblings(".selected, .move-right")
        .toggleClass("focus-on-left", bool);
}

function nextSlide(container, dots, n) {
    var visibleSlide = container.find(".cd-item-wrapper .selected"),
        navigationDot = container.find(".cd-dots .selected");
    if (typeof n === "undefined") n = visibleSlide.index() + 1;
    visibleSlide.removeClass("selected");
    container
        .find(".cd-item-wrapper li")
        .eq(n)
        .addClass("selected")
        .removeClass("move-right hover")
        .prevAll()
        .removeClass("move-right move-left focus-on-right")
        .addClass("hide-left")
        .end()
        .prev()
        .removeClass("hide-left")
        .addClass("move-left")
        .end()
        .next()
        .addClass("move-right");
    navigationDot.removeClass("selected");
    dots.eq(n).addClass("selected");
}

function prevSlide(container, dots, n) {
    var visibleSlide = container.find(".cd-item-wrapper .selected"),
        navigationDot = container.find(".cd-dots .selected");
    if (typeof n === "undefined") n = visibleSlide.index() - 1;
    visibleSlide.removeClass("selected focus-on-left");
    container
        .find(".cd-item-wrapper li")
        .eq(n)
        .addClass("selected")
        .removeClass("move-left hide-left hover")
        .nextAll()
        .removeClass("hide-left move-right move-left focus-on-left")
        .end()
        .next()
        .addClass("move-right")
        .end()
        .prev()
        .removeClass("hide-left")
        .addClass("move-left");
    navigationDot.removeClass("selected");
    dots.eq(n).addClass("selected");
}

function updatePrice(container, n) {
    var priceTag = container.find(".cd-price"),
        selectedItem = container.find(".cd-item-wrapper li").eq(n);
    if (selectedItem.data("sale")) {
        // if item is on sale - cross old price and add new one
        priceTag.addClass("on-sale");
        var newPriceTag =
            priceTag.next(".cd-new-price").length > 0 ?
            priceTag.next(".cd-new-price") :
            $('<em class="cd-new-price"></em>').insertAfter(priceTag);
        newPriceTag.text(selectedItem.data("price"));
        setTimeout(function() {
            newPriceTag.addClass("is-visible");
        }, 100);
    } else {
        // if item is not on sale - remove cross on old price and sale price
        priceTag
            .removeClass("on-sale")
            .next(".cd-new-price")
            .removeClass("is-visible")
            .on(
                "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
                function() {
                    priceTag.next(".cd-new-price").remove();
                }
            );
    }
}
//////////////////
// Main Carousel
//////////////////

//////////////////
// Owl Carousel
//////////////////
$(".owl-carousel").owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    singleItem: true,
    navText: [
        "<i class='fa fa-2x fa-caret-left'></i>",
        "<i class='fa fa-2x fa-caret-right'></i>",
    ],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
        },
        300: {
            items: 1,
        },
        600: {
            items: 2,
        },
        1000: {
            items: 4,
        },
    },
});

///////////////////
/// Menu Search
///////////////////
$(".dropdown-toggle").dropdown();

//////////////////
/// Range Slider
//////////////////
$(".js-range-slider").ionRangeSlider({
    skin: "round",
    step: 10,
    type: "double",
    grid: true,
    min: 0,
    max: 70000,
    from: 200,
    to: 40000,
    prefix: "GHS",
});

//////////////////////
//// Support Modal
//////////////////////
$("#email_support").on("show.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient = button.data("email"); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find(".modal-title").text("New Support Message to " + recipient);
    modal.find(".modal-body #recipient-name").val(recipient);
});

//////////////////////
//// Scroll to Top/Bottom
//////////////////////
$("body").append(
    '<a id="scrollTop" class="btn btn-samsung">Back to Top <i class="fas fa-arrow-up"></i></a>'
    // , '<a id="scrollBottom"  class="btn btn-samsung animate fadeIn">Scroll Down <i class="fas fa-arrow-down"></i></a>'
);

$(window).scroll(function() {
    if ($(this).scrollTop() != 0) {
        $("#scrollTop").fadeIn();
    } else {
        $("#scrollTop").fadeOut();
    }
    // if ($(this).scrollTop() > 0) {
    //     $("#scrollBottom").fadeOut();
    // } else {
    //     $("#scrollBottom").fadeIn();
    // }
});

$("#scrollTop").click(function() {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
});
// $("#scrollBottom").click(function() {
//     $("html, body").animate({ scrollTop: $(document).height() }, 1000);
//     return false;
// });

/////////////////////////////
/// Compare config
/////////////////////////////
jQuery(document).ready(function($) {
    var list = [];

    /* function to be executed when product is selected for comparision*/

    // $(document).on("click", ".addToCompare", function () {
    // 	$(".comparePanle").show();
    // 	$(this).parents(".selectProduct").toggleClass("is_selected");
    // 	var productID = $(this).parents(".selectProduct").attr("data-title");

    // 	var inArray = $.inArray(productID, list);
    // 	if (inArray < 0) {
    // 		if (list.length > 2) {
    // 			$("#WarningModal").show();
    // 			$("#warningModalClose").click(function () {
    // 				$("#WarningModal").hide();
    // 			});
    // 			$(this).parents(".selectProduct").toggleClass("is_selected");
    // 			return;
    // 		}

    // 		if (list.length < 3) {
    // 			list.push(productID);

    // 			var displayTitle = $(this).parents(".selectProduct").attr("data-id");

    // 			var image = $(this).siblings(".productImg").attr("src");

    // 			$(".comparePan").append(
    // 				'<div id="' +
    // 					productID +
    // 					'" class="relPos titleMargin mb-3 col"><div class="text-danger"><a class="selectedItemCloseBtn btn btn-danger text-white">Delete</a><img src="' +
    // 					image +
    // 					'" alt="' +
    // 					displayTitle +
    // 					'" style="height:150px;"/><p id="' +
    // 					productID +
    // 					'" class="titleMargin1">' +
    // 					displayTitle +
    // 					"</p></div></div>"
    // 			);
    // 		}
    // 	} else {
    // 		list.splice($.inArray(productID, list), 1);
    // 		var prod = productID.replace(" ", "");
    // 		$("#" + prod).remove();
    // 		hideComparePanel();
    // 	}
    // 	if (list.length > 1) {
    // 		$(".cmprBtn").addClass("active");
    // 		$(".cmprBtn").removeAttr("disabled");
    // 	} else {
    // 		$(".cmprBtn").removeClass("active");
    // 		$(".cmprBtn").attr("disabled", "");
    // 	}
    // });
    /*function to be executed when compare button is clicked*/
    $(document).on("click", ".cmprBtn", function() {
        if ($(".cmprBtn").hasClass("active")) {
            /* this is to print the  features list statically*/
            $(".contentPop").append(
                '<div class="col-3 compareItemParent relPos">' +
                '<ul class="product">' +
                '<li class=" relPos compHeader"><p class="lead">Features</p></li>' +
                "<li>Title</li>" +
                "<li>Size</li>" +
                "<li>Weight</li>" +
                '<li class="cpu">Processor</li>' +
                "<li>Battery</li></ul>" +
                "</div>"
            );

            for (var i = 0; i < list.length; i++) {
                /* this is to add the items to popup which are selected for comparision */
                product = $('.selectProduct[data-title="' + list[i] + '"]');
                var image = $("[data-title=" + list[i] + "]")
                    .find(".productImg")
                    .attr("src");
                var title = $("[data-title=" + list[i] + "]").attr("data-id");
                /*appending to div*/
                $(".contentPop").append(
                    '<div class="col compareItemParent relPos">' +
                    '<ul class="product">' +
                    '<li class="compHeader"><img src="' +
                    image +
                    '" class="compareThumb"></li>' +
                    "<li>" +
                    title +
                    "</li>" +
                    "<li>" +
                    $(product).data("size") +
                    "</li>" +
                    "<li>" +
                    $(product).data("weight") +
                    '<li class="cpu">' +
                    $(product).data("processor") +
                    "</li>" +
                    "<li>" +
                    $(product).data("battery") +
                    "</ul>" +
                    "</div>"
                );
            }
        }
        $(".modPos").show();
    });

    /* function to close the comparision popup */
    $(document).on("click", ".closeBtn", function() {
        $(".contentPop").empty();
        $(".comparePan").empty();
        $(".comparePanle").hide();
        $(".modPos").hide();
        $(".selectProduct").removeClass("is_selected");
        $(".cmprBtn").attr("disabled", "");
        list.length = 0;
    });

    /*function to remove item from preview panel*/
    $(document).on("click", ".selectedItemCloseBtn", function() {
        var test = $(this).siblings("p").attr("id");
        $("[data-title=" + test + "]")
            .find(".addToCompare")
            .click();
        hideComparePanel();
    });

    function hideComparePanel() {
        if (!list.length) {
            $(".comparePan").empty();
            $(".comparePanle").hide();
        }
    }
});


//////////////////////
///// blog Image gallery
/////////////////////
$(".img-wrapper").hover(
    function() {
        $(this).find(".img-overlay").animate({ opacity: 1 }, 600);
    },
    function() {
        $(this).find(".img-overlay").animate({ opacity: 0 }, 600);
    }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $(
    '<div id="prevButton"><i class="fa fa-chevron-left"></i></div>'
);
var $nextButton = $(
    '<div id="nextButton"><i class="fa fa-chevron-right"></i></div>'
);
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay
    .append($image)
    .prepend($prevButton)
    .append($nextButton)
    .append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
    // Prevents default behavior
    event.preventDefault();
    // Adds href attribute to variable
    var imageLocation = $(this).prev().attr("href");
    // Add the image src to $image
    $image.attr("src", imageLocation);
    // Fade in the overlay
    $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
    // Fade out the overlay
    $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
    // Hide the current image
    $("#overlay img").hide();
    // Overlay image location
    var $currentImgSrc = $("#overlay img").attr("src");
    // Image with matching location of the overlay image
    var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
    // Finds the next image
    var $nextImg = $($currentImg.closest(".image").next().find("img"));
    // All of the images in the gallery
    var $images = $("#image-gallery img");
    // If there is a next image
    if ($nextImg.length > 0) {
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
    } else {
        // Otherwise fade in the first image
        $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
    }
    // Prevents overlay from being hidden
    event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
    // Hide the current image
    $("#overlay img").hide();
    // Overlay image location
    var $currentImgSrc = $("#overlay img").attr("src");
    // Image with matching location of the overlay image
    var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
    // Finds the next image
    var $nextImg = $($currentImg.closest(".image").prev().find("img"));
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
    // Prevents overlay from being hidden
    event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
    // Fade out the overlay
    $("#overlay").fadeOut("slow");
});


// Compare Page Searchbox
jQuery(document).ready(function($) {
    function productsTable(element) {
        this.element = element;
        this.table = this.element.children(".cd-products-table");
        this.tableHeight = this.table.height();
        this.productsWrapper = this.table.children(".cd-products-wrapper");
        this.tableColumns = this.productsWrapper.children(".cd-products-columns");
        this.products = this.tableColumns.children(".product");
        this.productsNumber = this.products.length;
        this.productWidth = this.products.eq(0).width();
        this.productsTopInfo = this.table.find(".top-info");
        this.featuresTopInfo = this.table
            .children(".features")
            .children(".top-info");
        this.topInfoHeight = this.featuresTopInfo.innerHeight() + 30;
        this.leftScrolling = false;
        this.filterBtn = this.element.find(".filter");
        this.resetBtn = this.element.find(".reset");
        (this.filtering = false), (this.selectedproductsNumber = 0);
        this.filterActive = false;
        this.navigation = this.table.children(".cd-table-navigation");
        // bind table events
        this.bindEvents();
    }

    productsTable.prototype.bindEvents = function() {
        var self = this;
        //detect scroll left inside producst table
        self.productsWrapper.on("scroll", function() {
            if (!self.leftScrolling) {
                self.leftScrolling = true;
                !window.requestAnimationFrame ?
                    setTimeout(function() {
                        self.updateLeftScrolling();
                    }, 250) :
                    window.requestAnimationFrame(function() {
                        self.updateLeftScrolling();
                    });
            }
        });
        //select single product to filter
        self.products.on("click", ".top-info", function() {
            var product = $(this).parents(".product");
            if (!self.filtering && product.hasClass("selected")) {
                product.removeClass("selected");
                self.selectedproductsNumber = self.selectedproductsNumber - 1;
                self.upadteFilterBtn();
            } else if (!self.filtering && !product.hasClass("selected")) {
                product.addClass("selected");
                self.selectedproductsNumber = self.selectedproductsNumber + 1;
                self.upadteFilterBtn();
            }
        });
        //filter products
        self.filterBtn.on("click", function(event) {
            event.preventDefault();
            if (self.filterActive) {
                self.filtering = true;
                self.showSelection();
                self.filterActive = false;
                self.filterBtn.removeClass("active");
            }
        });
        //reset product selection
        self.resetBtn.on("click", function(event) {
            event.preventDefault();
            if (self.filtering) {
                self.filtering = false;
                self.resetSelection();
            } else {
                self.products.removeClass("selected");
            }
            self.selectedproductsNumber = 0;
            self.upadteFilterBtn();
        });
        //scroll inside products table
        this.navigation.on("click", "a", function(event) {
            event.preventDefault();
            self.updateSlider($(event.target).hasClass("next"));
        });
    };

    productsTable.prototype.upadteFilterBtn = function() {
        //show/hide filter btn
        if (this.selectedproductsNumber >= 2) {
            this.filterActive = true;
            this.filterBtn.addClass("active");
        } else {
            this.filterActive = false;
            this.filterBtn.removeClass("active");
        }
    };

    productsTable.prototype.updateLeftScrolling = function() {
        var totalTableWidth = parseInt(this.tableColumns.eq(0).outerWidth(true)),
            tableViewport = parseInt(this.element.width()),
            scrollLeft = this.productsWrapper.scrollLeft();

        scrollLeft > 0 ?
            this.table.addClass("scrolling") :
            this.table.removeClass("scrolling");

        if (this.table.hasClass("top-fixed") && checkMQ() == "desktop") {
            setTranformX(this.productsTopInfo, "-" + scrollLeft);
            setTranformX(this.featuresTopInfo, "0");
        }

        this.leftScrolling = false;

        this.updateNavigationVisibility(scrollLeft);
    };

    productsTable.prototype.updateNavigationVisibility = function(scrollLeft) {
        scrollLeft > 0 ?
            this.navigation.find(".prev").removeClass("inactive") :
            this.navigation.find(".prev").addClass("inactive");
        scrollLeft <
            this.tableColumns.outerWidth(true) - this.productsWrapper.width() &&
            this.tableColumns.outerWidth(true) > this.productsWrapper.width() ?
            this.navigation.find(".next").removeClass("inactive") :
            this.navigation.find(".next").addClass("inactive");
    };

    productsTable.prototype.updateTopScrolling = function(scrollTop) {
        var offsetTop = this.table.offset().top,
            tableScrollLeft = this.productsWrapper.scrollLeft();

        if (
            offsetTop <= scrollTop &&
            offsetTop + this.tableHeight - this.topInfoHeight >= scrollTop
        ) {
            //fix products top-info && arrows navigation
            if (!this.table.hasClass("top-fixed") &&
                $(document).height() > offsetTop + $(window).height() + 200
            ) {
                this.table.addClass("top-fixed").removeClass("top-scrolling");
                if (checkMQ() == "desktop") {
                    this.productsTopInfo.css("top", "0");
                    this.navigation.find("a").css("top", "0px");
                }
            }
        } else if (offsetTop <= scrollTop) {
            //product top-info && arrows navigation -  scroll with table
            this.table.removeClass("top-fixed").addClass("top-scrolling");
            if (checkMQ() == "desktop") {
                this.productsTopInfo.css(
                    "top",
                    this.tableHeight - this.topInfoHeight + "px"
                );
                this.navigation
                    .find("a")
                    .css("top", this.tableHeight - this.topInfoHeight + "px");
            }
        } else {
            //product top-info && arrows navigation -  reset style
            this.table.removeClass("top-fixed top-scrolling");
            this.productsTopInfo.attr("style", "");
            this.navigation.find("a").attr("style", "");
        }

        this.updateLeftScrolling();
    };

    productsTable.prototype.updateProperties = function() {
        this.tableHeight = this.table.height();
        this.productWidth = this.products.eq(0).width();
        this.topInfoHeight = this.featuresTopInfo.innerHeight() + 30;
        this.tableColumns.css(
            "width",
            this.productWidth * this.productsNumber + "px"
        );
    };

    productsTable.prototype.showSelection = function() {
        this.element.addClass("filtering");
        this.filterProducts();
    };

    productsTable.prototype.resetSelection = function() {
        this.tableColumns.css(
            "width",
            this.productWidth * this.productsNumber + "px"
        );
        this.element.removeClass("no-product-transition");
        this.resetProductsVisibility();
    };

    productsTable.prototype.filterProducts = function() {
        var self = this,
            containerOffsetLeft = self.tableColumns.offset().left,
            scrollLeft = self.productsWrapper.scrollLeft(),
            selectedProducts = this.products.filter(".selected"),
            numberProducts = selectedProducts.length;

        selectedProducts.each(function(index) {
            var product = $(this),
                leftTranslate =
                containerOffsetLeft +
                index * self.productWidth +
                scrollLeft -
                product.offset().left;
            setTranformX(product, leftTranslate);

            if (index == numberProducts - 1) {
                product.one(
                    "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
                    function() {
                        setTimeout(function() {
                            self.element.addClass("no-product-transition");
                        }, 50);
                        setTimeout(function() {
                            self.element.addClass("filtered");
                            self.productsWrapper.scrollLeft(0);
                            self.tableColumns.css(
                                "width",
                                self.productWidth * numberProducts + "px"
                            );
                            selectedProducts.attr("style", "");
                            product.off(
                                "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend"
                            );
                            self.updateNavigationVisibility(0);
                        }, 100);
                    }
                );
            }
        });

        if ($(".no-csstransitions").length > 0) {
            //browser not supporting css transitions
            self.element.addClass("filtered");
            self.productsWrapper.scrollLeft(0);
            self.tableColumns.css("width", self.productWidth * numberProducts + "px");
            selectedProducts.attr("style", "");
            self.updateNavigationVisibility(0);
        }
    };

    productsTable.prototype.resetProductsVisibility = function() {
        var self = this,
            containerOffsetLeft = self.tableColumns.offset().left,
            selectedProducts = this.products.filter(".selected"),
            numberProducts = selectedProducts.length,
            scrollLeft = self.productsWrapper.scrollLeft(),
            n = 0;

        self.element.addClass("no-product-transition").removeClass("filtered");

        self.products.each(function(index) {
            var product = $(this);
            if (product.hasClass("selected")) {
                n = n + 1;
                var leftTranslate = (-index + n - 1) * self.productWidth;
                setTranformX(product, leftTranslate);
            }
        });

        setTimeout(function() {
            self.element.removeClass("no-product-transition filtering");
            setTranformX(selectedProducts, "0");
            selectedProducts.removeClass("selected").attr("style", "");
        }, 50);
    };

    productsTable.prototype.updateSlider = function(bool) {
        var scrollLeft = this.productsWrapper.scrollLeft();
        scrollLeft = bool ?
            scrollLeft + this.productWidth :
            scrollLeft - this.productWidth;

        if (scrollLeft < 0) scrollLeft = 0;
        if (
            scrollLeft >
            this.tableColumns.outerWidth(true) - this.productsWrapper.width()
        )
            scrollLeft =
            this.tableColumns.outerWidth(true) - this.productsWrapper.width();

        this.productsWrapper.animate({ scrollLeft: scrollLeft }, 200);
    };

    var comparisonTables = [];
    $(".cd-products-comparison-table").each(function() {
        //create a productsTable object for each .cd-products-comparison-table
        comparisonTables.push(new productsTable($(this)));
    });

    var windowScrolling = false;
    //detect window scroll - fix product top-info on scrolling
    $(window).on("scroll", function() {
        if (!windowScrolling) {
            windowScrolling = true;
            !window.requestAnimationFrame ?
                setTimeout(checkScrolling, 250) :
                window.requestAnimationFrame(checkScrolling);
        }
    });

    var windowResize = false;
    //detect window resize - reset .cd-products-comparison-table properties
    $(window).on("resize", function() {
        if (!windowResize) {
            windowResize = true;
            !window.requestAnimationFrame ?
                setTimeout(checkResize, 250) :
                window.requestAnimationFrame(checkResize);
        }
    });

    function checkScrolling() {
        var scrollTop = $(window).scrollTop();
        comparisonTables.forEach(function(element) {
            element.updateTopScrolling(scrollTop);
        });

        windowScrolling = false;
    }

    function checkResize() {
        comparisonTables.forEach(function(element) {
            element.updateProperties();
        });

        windowResize = false;
    }

    function checkMQ() {
        //check if mobile or desktop device
        return window
            .getComputedStyle(comparisonTables[0].element.get(0), "::after")
            .getPropertyValue("content")
            .replace(/'/g, "")
            .replace(/"/g, "");
    }

    function setTranformX(element, value) {
        element.css({
            "-moz-transform": "translateX(" + value + "px)",
            "-webkit-transform": "translateX(" + value + "px)",
            "-ms-transform": "translateX(" + value + "px)",
            "-o-transform": "translateX(" + value + "px)",
            transform: "translateX(" + value + "px)",
        });
    }
});

// Compare Page Search bar
$(document).ready(function() {
    var submitIcon = $(".searchbar-icon");
    var inputBox = $(".searchbar-input");
    var searchbar = $(".searchbar");
    var isOpen = false;
    submitIcon.click(function() {
        if (isOpen == false) {
            searchbar.addClass("searchbar-open");
            inputBox.focus();
            isOpen = true;
        } else {
            searchbar.removeClass("searchbar-open");
            inputBox.focusout();
            isOpen = false;
        }
    });
    submitIcon.mouseup(function() {
        return false;
    });
    searchbar.mouseup(function() {
        return false;
    });
    $(document).mouseup(function() {
        if (isOpen == true) {
            $(".searchbar-icon").css("display", "block");
            submitIcon.click();
        }
    });
});

function buttonUp() {
    var inputVal = $(".searchbar-input").val();
    inputVal = $.trim(inputVal).length;
    if (inputVal !== 0) {
        $(".searchbar-icon").css("display", "none");
    } else {
        $(".searchbar-input").val("");
        $(".searchbar-icon").css("display", "block");
    }
}

// Disabling form submissions if there are invalid fields
(function() {
    "use strict";

    window.addEventListener(
        "load",
        function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName("needs-validation");

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener(
                    "submit",
                    function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add("was-validated");
                    },
                    false
                );
            });
        },
        false
    );
})();

// Home image class change for small devices
$(document).ready(function() {
    if ($(window).width() <= 768) {
        $("#Home-Alt").addClass("home-mobile");
        $("#Home-Alt").removeClass("home-large");
    } else {
        $("#Home-Alt").addClass("home-large");
        $("#Home-Alt").removeClass("home-mobile");
    }
});