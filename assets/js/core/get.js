"use strict"; 

var baseUrl = atob(sessionStorage.getItem('ip'))+"api/"; 
var branchId = atob(sessionStorage.getItem('branchId')); 
var userId = atob(sessionStorage.getItem('userId')), bUrl = atob(sessionStorage.getItem('ip'));
var auth = atob(sessionStorage.getItem('auth')), rout = atob(sessionStorage.getItem('rout'));

var pathArray = window.location.pathname.split( '/' ); 
var Case = location.pathname.split('/').slice(-1)[0]; 
var Case1 = location.pathname.split('/').slice(-2)[0]; 

var loc = Case.split('.').slice()[0];

$(document).ready(function() {
    //console.log(loc);
    if (loc != 'Login') { 
        //console.log('getSession');
        //getSession();
    }
    siteController(loc); var i;
    for (i = 0; i < pathArray.length; i++) {
      if(pathArray[i] !== ""){ $('.'+pathArray[i]).addClass('lazur-bg'); }
    }
});

function siteController(Rout){
    switch (Rout) {
        case "Dashboard":
            {

                break;
            };
        case "Blog":
            {
                getBlogs();
                break;
            };
        case "Event":
            {
                getEvents();
                break;
            };
        case "Video":
            {
                getVideos();
                break;
            };
        case "PodCast":
            {
                getPodCasts();
                break;
            };
        case "Image":
            {
                getImages();
                break;
            };
        case "Subcribe":
            {
                getSubcribe();
                break;
            };
        }
}

function hidden(){
    $('.Addform').addClass('hidden');
}

function getSession(){
    if (sessionStorage.getItem('userId') != null){
        $.ajax({
            type: 'GET',
            async: false,
            url: baseUrl +'Users/' + userId,
            dataType: 'json',
            headers: {
                'Authorization' : 'Bearer ' + auth
            },
            success: function(data){ //console.log(data.Privillege);
                $('#userfullname').html(data.FullName);
                $('#Postedby').val(data.FullName);
                sessionStorage.setItem('companyId', btoa(data.CompanyId));
                sessionStorage.setItem('user', btoa(data.FullName));
                $('#UserPic').attr("src", bUrl+"Files/User/"+data.Image ).width(90).height(90);
            //$('#UserPic').attr('src', "http://godfreddavidson-002-site7.ftempurl.com/Files/User/"+data.Image).width(90).height(90);
            },
            error: function(jqXHR){
                checkStatus(jqXHR.status);
            }
        });       
    }else{
        console.log('redirecting');
        window.location.href = "Login";
    }
}

function checkStatus(status){
    switch (status) {
        case 401:
            {
                console.log(status);
                logOut();
                break;
            };
        case 500:
            {
                console.log(status);
                break;
            };
        case 501:
            {
                console.log(status);
                break;
            };
        }
}

function logOut(){ 
    $.ajax({
        type: 'GET',
        url: baseUrl +'Users/' + userId,
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){
            $.ajax({
                type: 'POST',
                url: baseUrl +'Logout/'+ data.Username,
                dataType: 'json',
                headers: {
                    'Authorization' : 'Bearer ' + auth
                },
                success: function (data) {
                    displayMsg("success", 'logOut Successfull', 'You have been Logout Successfully');
                    sessionStorage.clear();
                    window.location.href = "Login";
                },
                error: function(jqXHR){
                    sessionStorage.clear();
                    window.location.href = "Login";
                }
            }); 
        },
        error: function(jqXHR){
            sessionStorage.clear();
            window.location.href = "Login";
        }
    });
}

function read(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#Pass')
                .attr('src', e.target.result)
                .width(300)
                .height(250);
            };
        reader.readAsDataURL(input.files[0]);
    };
}

function getBlogs(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphBlogs/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){
            var indList = $('#AsaphBlogs').DataTable(); var c=1;

            $.each(data, function(key, value) {
                var dob = formatDate(value.CreatedDate);
                indList.row.add( [ c, value.BlogNo,value.Image,value.Postedby,dob,'<a onclick="editBlog('+value.BlogId+')" class="btn btn-xs btn-warning">Edit</a> <a onclick="delBlog('+value.BlogId+')" class="btn btn-xs btn-danger">Delete</a>'] ).draw(false)
                c++;
            });
        },
        error: function(jqXHR){
            checkStatus(jqXHR.status);
        }
    });
}

function getEvents(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphEvents/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){ //console.log(data);
            var indList = $('#AsaphEvent').DataTable(); var c=1;
            $.each(data, function(key, value) {
                var cd = formatDate(value.CreatedDate);;
                indList.row.add( [ c,value.Title,value.EventDate,value.EndDate,value.Venue,value.Status,cd,'<a onclick="editEvent('+value.EventId+')" class="btn btn-xs btn-warning">Edit</a> <a onclick="delEvent('+value.EventId+')" class="btn btn-xs btn-danger">Delete</a>' ] ).draw(false);
                c++;
            });
        },
        error: function(jqXHR){
            //checkStatus(jqXHR.status);
        }
    });
}

function getImages(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphImages/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){
            var payList = $('#AsaphImage').DataTable(); var c=1;
            $.each(data, function(key, value) {
                var cd = formatDate(value.CreatedDate);
                payList.row.add( [ c,value.Title,value.Name,cd,'<a onclick="editImage('+value.ImageId+')" class="btn btn-xs btn-warning">Edit</a> <a onclick="delImage('+value.ImageId+')" class="btn btn-xs btn-danger">Delete</a>' ] ).draw(false);
                c++;
            });
        },
        error: function(jqXHR){
            checkStatus(jqXHR.status);
        }
    });
}

function getPodCasts(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphPodCasts/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){ //console.log(data);
            var payList = $('#AsaphAudio').DataTable(); var c=1;
            $.each(data, function(key, value) { //console.log(value);
                var cd = formatDate(value.CreatedDate);
                payList.row.add( [ c,value.PodCastNo,value.AudioUrl,cd,'<a onclick="editPodCast('+value.PodCastId+')" class="btn btn-xs btn-warning">Edit</a> <a onclick="delPodCast('+value.PodCastId+')" class="btn btn-xs btn-danger">Delete</a>'] ).draw(false);
                c++;
            });
        },
        error: function(jqXHR){
            checkStatus(jqXHR.status);
        }
    });
}

function getVideos(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphVideos/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){ //console.log(data);
            var payList = $('#AsaphVideo').DataTable(); var c = 1;
            $.each(data, function(key, value) { //console.log(value);
                var cd  = formatDate(value.CreatedDate);
                payList.row.add( [ c,value.VideoNo,value.VideoUrl,cd,'<a onclick="editVideo('+value.VideoId+')" class="btn btn-xs btn-warning">Edit</a> <a onclick="delVideo('+value.VideoId+')" class="btn btn-xs btn-danger">Delete</a>' ] ).draw(false);
            });
        },
        error: function(jqXHR){
            checkStatus(jqXHR.status);
        }
    });
}

function getSubcribe(){
    $.ajax({
        type: 'GET',
        url: baseUrl +'AsaphSubcribe/',
        dataType: 'json',
        headers: {
            'Authorization' : 'Bearer ' + auth
        },
        success: function(data){ //console.log(data);
            var payList = $('#Subcribers').DataTable(); var c = 1;
            $.each(data, function(key, value) { //console.log(value);
                payList.row.add( [ c,value.Email,value.CreatedDate,'<a onclick="delSubcribe('+value.SubcribeId+')" class="btn btn-xs btn-danger">Delete</a>' ] ).draw(false);
            });
        },
        error: function(jqXHR){
            checkStatus(jqXHR.status);
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [ day, month, year].join('-');
}


