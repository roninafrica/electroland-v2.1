var Urls = atob(sessionStorage.getItem('ip')),
    limit = 4; //console.log(cityId);

var apiurl = "https://electrolandgh.roninafrica.com/api/website/";
var imgurl = "https://electrolandgh.roninafrica.com/public/";
// var apiurl = "http://localhost:4000/api/website/";

let products = JSON.parse(localStorage.getItem("compare")) || [];
let cart = JSON.parse(localStorage.getItem("cart")) || [];
// if (!products) { products = []; }

var headers = { "Content-Type": "application/json", };
// var compare = localStorage.getItem('compare');
var mobile = false;
if (isMobile() || isMobileTablet()) { mobile = true; }


// var app = angular.module('myApp', []);
var app = angular.module('myApp', ['ngSanitize'] /*['ngAnimate']*/ );

app.run(function($rootScope, $http) {
    $rootScope.date = new Date();
    $rootScope.imageurl = imgurl;
    $rootScope.compare = products;
    $rootScope.cart = cart;
    $rootScope.mobile = mobile;
    console.log(products);
    // checkStatus(1);

    $rootScope.addToCompare = function(dat) {
        // var prod = [];
        // if ($rootScope.compare) { prod.push($rootScope.compare); }
        // prod.push(dat);
        $rootScope.compare.push(dat)
        console.log(dat);
        localStorage.setItem("compare", JSON.stringify($rootScope.compare));
        checkStatus(1, dat);
    }
    $rootScope.addToCart = function(dat) {
        // var prod = [];
        var item = { product: dat, quantity: 1 };
        var check = cartarry($rootScope.cart, dat._id);
        console.log(check);
        if (check && check.length > 0 && check[0].product._id == dat._id) {
            $rootScope.cart.find((items, index) => {
                if (items.product._id == dat._id) {
                    var qty = $rootScope.cart[index].quantity + item.quantity;
                    if (qty > 0) {
                        $rootScope.cart[index].quantity = qty;
                        localStorage.setItem("cart", JSON.stringify($rootScope.cart));
                        checkStatus(3, dat);
                        console.log('updated')
                    }
                    console.log('updating')
                }
            });
            console.log('update');
            // var ite = cartarry($rootScope.cart, dat._id)
            // const index = products.indexOf(item);
            // products.splice(index, 1);
            // $rootScope.cart.push(item)
            // console.log(dat);
            // localStorage.setItem("cart", JSON.stringify($rootScope.cart));
        } else {
            $rootScope.cart.push(item)
            console.log(dat);
            localStorage.setItem("cart", JSON.stringify($rootScope.cart));
            checkStatus(2, dat);
            console.log('added');
        }
    }
    $rootScope.reset = function() {
        localStorage.removeItem('compare');
        $roorScope.compare = [];
    }
    $rootScope.remove = function(dat) {
        $rootScope.compare = arrayRemove($rootScope.compare, dat._id)
        localStorage.setItem("compare", JSON.stringify($rootScope.compare));
    }
    $rootScope.removecart = function(dat) {
        $rootScope.cart = cartarryremove($rootScope.cart, dat.product._id)
        localStorage.setItem("cart", JSON.stringify($rootScope.cart));
    }
    $rootScope.addToCarts = function(product, quantity) {
        // var item: CartItem | boolean = false;
        // If Products exist
        let hasItem = products.find((items, index) => {
            if (items.product.id == product.id) {
                let qty = products[index].quantity + quantity;
                let stock = $rootScope.calculateStockCounts(products[index], quantity);
                if (qty != 0 && stock) {
                    products[index]['quantity'] = qty;
                    toastr.success('This product has been added.');
                }
                return true;
            }
        });
        // If Products does not exist (Add New Products)
        if (!hasItem) {
            item = { product: product, quantity: quantity };
            products.push(item);
            toastr.success('This product has been added.');
        }

        localStorage.setItem("cartItem", JSON.stringify(products));
        return item;
    }

    // Update Cart Value
    $rootScope.updateCartQuantity = function(product, quantity) {
        return products.find((items, index) => {
            if (items.product.id == product.id) {
                var qty = products[index].quantity + quantity;
                var stock = $rootScope.calculateStockCounts(products[index], quantity);
                if (qty != 0 && stock)
                    products[index]['quantity'] = qty;
                localStorage.setItem("cart", JSON.stringify(products));
                return true;
            }
        });
    }

    $rootScope.calculateStockCounts = function(product, quantity) {
        let qty = product.quantity + quantity;
        let stock = product.product.stock;
        if (stock < qty) {
            toastr.error('You can not add more items than available. In stock ' + stock + ' items.');
            return false
        }
        return true
    }

    // Removed in cart
    $rootScope.removeFromCart = function(item) {
        if (item === undefined) return false;
        const index = products.indexOf(item);
        products.splice(index, 1);
        localStorage.setItem("cart", JSON.stringify(products));
    }

    // Total amount 
    $rootScope.getTotalAmount = function() {
        var amount = 0;
        $rootScope.cart.find((items, index) => {
            var amt = items.product.current_amount * items.quantity;
            amount += amt;
        });

        return amount;
        // return $rootScope.cart.pipe(map((product) => {
        //     return $rootScope.cart.reduce((prev, curr) => {
        //         return prev + curr.product.price * curr.quantity;
        //     }, 0);
        // }));
    }
    $rootScope.total = function() {
            return 1000
        }
        //place  Order 
    $rootScope.postOrder = function() {
        $http.post(apiurl + "orders", { headers: headers }).then(function(response) {
            $rootScope.order = response.data;
        });
    }
    $rootScope.getOrder = function() {
        var pathArray = window.location.pathname.split('/');
        $http.get(apiurl + "orders/" + pathArray[pathArray.length - 1], { headers: headers }).then(function(response) {
            $rootScope.order = response.data;
        });
    }
});

app.filter('capitalize', function() {
    return function(input, all) {
        var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
        return (!!input) ? input.replace(reg, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); }) : '';
    }
});


app.controller('searchcontroller', function($scope, $http) {
    $http.get(apiurl + "category", { headers: headers }).then(function(response) {
        $scope.category = response.data;
    });
    $http.get(apiurl + "products/all", { headers: headers }).then(function(response) {
        $scope.products = response.data;
    });
});


app.controller('homecontroller', function($scope, $http) {
    $http.get(apiurl + "category", { headers: headers }).then(function(response) {
        $scope.category = response.data;
    });
    $http.get(apiurl + "brands", { headers: headers }).then(function(response) {
        $scope.brands = response.data;
    });
});

app.controller('productcontroller', function($scope, $http, $sce) {
    var pathArray = window.location.pathname.split('/');
    console.log(pathArray, pathArray[pathArray.length - 1]);
    $http.get(apiurl + pathArray[pathArray.length - 1], { headers: headers }).then(function(response) {
        $scope.product = response.data;
        console.log(response.data);
    });
    $scope.productData = function(dat) {
        $scope.product = dat;
        // console.log(dat);
    }
    $scope.selectedSize = function(data) {
        console.log(data);
        $scope.product.code = data.code;
        $scope.product.current_amount = data.amount;
        $scope.product.imageurl = data.imageurl;
        $scope.product.link = data.link;
    }
});

app.controller('productscontroller', function($scope, $http) {
    $scope.count = 18;
    $scope.products = [];
    $scope.loader = true;
    $http.get(apiurl + 'products', { headers: headers }).then(function(response) {
        $scope.products = response.data;
        // console.log(response.data);
    });
    $http.get(apiurl + "brands/all", { headers: headers }).then(function(response) {
        $scope.bras = response.data;
    });
    $http.get(apiurl + "category", { headers: headers }).then(function(response) {
        $scope.cats = response.data;
    });
    $scope.productData = function(dat) {
        $scope.product = dat;
        // console.log(dat);
    }
    $scope.selectedSize = function(data) {
        console.log(data);
        $scope.product.code = data.code;
        $scope.product.current_amount = data.amount;
        $scope.product.imageurl = data.imageurl;
        $scope.product.link = data.link;
    }
    $scope.loadmore = function() {
        $scope.count += 6;
        if ($scope.products.length <= $scope.count) {
            $scope.loader = false;
        }
    }
});

app.controller('productcardcontroller', function($scope, $http) {
    // $http.get(apiurl + "category", { headers: headers }).then(function(response) {
    //     $scope.category = response.data;
    // });
    $scope.productData = function(data) {
        // console.log(data);
        $scope.product = data;
        if (data.imageurl) { $scope.product.imageurl = data.imageurl; }
    }
    $scope.selectedSize = function(data) {
        console.log(data);
        $scope.product.code = data.code;
        $scope.product.current_amount = data.amount;
        if (data.imageurl) { $scope.product.imageurl = data.imageurl; }
    }
});

app.controller('promotioncontroller', function($scope, $http) {
    $http.get(apiurl + "brands/all", { headers: headers }).then(function(response) {
        $scope.brands = response.data;
    });
    $scope.selectedbrand = function(data) {
        $scope.brand = data;
        $http.get(apiurl + "promotions/" + data._id, { headers: headers }).then(function(response) {
            $scope.brand = response.data[0];
        });
    }
});

app.controller('locationscontroller', function($scope, $http) {
    $scope.count = 3;
    $scope.loader = true
    var pos;
    $http.get(apiurl + "branches", { headers: headers }).then(function(response) {
        $scope.branches = response.data;
        console.log($scope.branches);
    });
    $scope.loadmore = function() {
        $scope.count += 3;
        if ($scope.branches.length <= $scope.count) {
            $scope.loader = false;
        }
    }

    navigator.geolocation.getCurrentPosition(function(position) {
        pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        console.info(pos);

        var myStorelocator = new storelocatorjs({
            options: {
                // Google API Key
                apiKey: 'AIzaSyAGBbfja7Mr3UXuJFizf89Q3fQbPY3XYqk',

                // web service to get JSON store datas
                webServiceUrl: apiurl + 'branches',

                // Marker Clustering Options
                cluster: {
                    options: {
                        averageCenter: true,
                        gridSize: 50,
                        imagePath: imgurl, // 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                        maxZoom: 16,
                        minimumClusterSize: 2,
                        styles: [],
                        zoomOnClick: true
                    },
                    status: false
                },

                // Debug mode
                debug: false,

                // geolocation locations
                geolocation: {
                    startOnLoad: true,
                    status: true
                },

                // Google Maps options
                map: {
                    markers: {
                        width: 30,
                        height: 40,
                        styles: [{
                            category: 'userPosition',
                            colorBackground: '#4285f4',
                            colorText: '#fff'
                        }]
                    },
                    options: {
                        center: [pos.lat, pos.lng],
                        disableDefaultUI: false,
                        fullscreenControl: true,
                        mapTypeControl: false,
                        mapTypeId: 'roadmap',
                        scaleControl: false,
                        scrollwheel: true,
                        streetViewControl: false,
                        styles: [],
                        zoom: 16
                    }
                },

                // Request options
                requests: {
                    searchRadius: 50,
                    storesLimit: 10
                },

                // Default selectors
                selectors: {
                    container: '.storelocator',
                    formSearch: '.storelocator-formSearch',
                    geolocButton: '.storelocator-geolocButton',
                    inputSearch: '.storelocator-inputSearch',
                    loader: '.storelocator-loader',
                    nav: '.storelocator-nav',
                    searchFilters: '[data-filter]',
                    sidebar: '.storelocator-sidebar',
                    sidebarResults: '.storelocator-sidebarResults'
                },

                // markers options
                markersUpdate: {
                    limitInViewport: 30,
                    maxRadius: 150,
                    status: true,
                    stepRadius: 50
                }
            },
            onReady: function(map) {
                this.triggerRequest({
                    'lat': pos.lat,
                    'lng': pos.lng
                });
            },
        });
    });

});

app.controller('blogscontroller', function($scope, $http) {
    $http.get(apiurl + "blogs", { headers: headers }).then(function(response) {
        $scope.blogs = response.data;
    });
});

app.controller('blogdetailscontroller', function($scope, $http) {
    $http.get(apiurl + "blogs", { headers: headers }).then(function(response) {
        $scope.blogs = response.data;
    });
});

app.controller('brandcategoryproducts', function($scope, $http, $location) {
    $scope.count = 18;
    $scope.products = [];
    $scope.loader = true;
    var pathArray = window.location.pathname.split('/');
    console.log(pathArray, pathArray[pathArray.length - 1]);
    $scope.categoryid = pathArray[pathArray.length - 1];
    $http.get(apiurl + "brands/" + pathArray[pathArray.length - 3], { headers: headers }).then(function(response) {
        $scope.b = response.data;
        $scope.cats = response.data.categories;
        console.log(response.data);
    });
    $http.get(apiurl + "brands/" + pathArray[pathArray.length - 3] + '/' + pathArray[pathArray.length - 1], { headers: headers }).then(function(response) {
        $scope.brandproducts = response.data.products;
        console.log($scope.brandproducts);
    });

    $scope.getData = function(value) {
        $http.get(apiurl + "brands/" + pathArray[pathArray.length - 3] + '/' + value._id, { headers: headers }).then(function(response) {
            $scope.brandproducts = response.data.products;
            // return response.data.products;
            // console.log($scope.brandproducts);
        });
    }
    $http.get(apiurl + "brands/all", { headers: headers }).then(function(response) {
        $scope.bras = response.data;
        console.log($scope.bras);
    });
    // $http.get(apiurl + "products", { headers: headers }).then(function(response) {
    //     // $scope.brandproducts = response.data;
    // });

    $scope.loadmore = function() {
        $scope.count += 6;
        if ($scope.products.length <= $scope.count) {
            $scope.loader = false;
        }
    }
});

app.controller('categoryproducts', function($scope, $http, $location) {
    $scope.samsung = [];
    $scope.toshiba = [];
    $scope.nasco = [];
    $scope.midea = [];
    $scope.abb = [];
    $scope.count = 10;
    $http.get(apiurl + "brands/all", { headers: headers }).then(function(response) {
        $scope.bras = response.data;
        $scope.bras.forEach(myFunction);

        function myFunction(value, index, array) {
            // console.log(value);
            $scope.getData(value, pathArray[pathArray.length - 1]);
            // if (value.name == 'samsung') {
            //     $scope.samsung = $scope.getData(value);
            // } else if (value.name == 'toshiba') {
            //     $scope.toshiba = $scope.getData(value);
            // } else if (value.name == 'midea') {
            //     $scope.midea = $scope.getData(value);
            // } else if (value.name == 'nasco') {
            //     $scope.nasco = $scope.getData(value);
            // } else if (value.name == 'abb') {
            //     $scope.abb = $scope.getData(value);
            // } else if (value.name == 'philips') {
            //     $scope.philips = $scope.getData(value);
            // }
        }
    });
    $http.get(apiurl + "fliters", { headers: headers }).then(function(response) {
        $scope.fliters = response.data;
    });
    $http.get(apiurl + "category", { headers: headers }).then(function(response) {
        $scope.cats = response.data;
    });
    var pathArray = window.location.pathname.split('/');
    // console.log(pathArray);
    $scope.categoryid = pathArray[pathArray.length - 1];
    $http.get(apiurl + "category/" + pathArray[pathArray.length - 1], { headers: headers }).then(function(response) {
        $scope.categoryproducts = response.data;
    });
    $scope.getProduct = function(val) {
        $scope.bras.forEach(myFunction);

        function myFunction(value, index, array) {
            // console.log(value);
            $scope.getData(value, val._id);
        }
    }
    $scope.getData = function(value, id) {
        $http.get(apiurl + "brands/" + value.name + '/' + id, { headers: headers }).then(function(response) {
            if (value.name.toLowerCase() == 'samsung') {
                $scope.samsung = response.data.products;
            } else if (value.name.toLowerCase() == 'toshiba') {
                $scope.toshiba = response.data.products;
            } else if (value.name == 'midea') {
                $scope.midea = response.data.products;
            } else if (value.name.toLowerCase() == 'nasco') {
                $scope.nasco = response.data.products;
            } else if (value.name == 'abb') {
                $scope.abb = response.data.products;
            } else if (value.name.toLowerCase() == 'philips') {
                $scope.philips = response.data.products;
            } else if (value.name.toLowerCase() == 'kodak') {
                $scope.kodak = response.data.products;
            } else if (value.name.toLowerCase() == 'sollatek') {
                $scope.sollatek = response.data.products;
            }
            // return response.data.products;
            // console.log($scope.brandproducts);
        });
    }

    $scope.loadmore = function() {
        $scope.count += 6;
    }
});

function isMobile() {
    var check = false;
    (function(a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

function isMobileTablet() {
    var check = false;
    (function(a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}


function arrayRemove(arr, val) {
    return arr.filter((ele) => {
        return ele._id != val;;
    });
}

function cartarry(arr, val) {
    if (arr.length < 1) return false;
    return arr.filter((ele) => {
        return ele.product._id == val;;
    });
}

function cartarryremove(arr, val) {
    return arr.filter((ele) => {
        return ele.product._id != val;;
    });
}

function displayMsg(type, title, text) {
    var type = type,
        title = title,
        text = text;
    swal({
        title: title,
        text: text,
        type: type
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
}

function getToken() {
    $.ajax({
        type: 'GET',
        async: false,
        url: Urls + '/AccessToken/',
        dataType: 'json',
        success: function(data) {
            auth = data.access_token;
        }
    });
    return auth;
}

function fileUpload(type, file) {
    var name, fil = file;
    var seq = type;
    var data = new FormData();
    console.log(seq);
    console.log(fil);
    if (fil.length > 0) {
        data.append("file", fil[0]);
    }
    $.ajax({
        type: "POST",
        url: Urls + "/Upload/" + seq,
        contentType: false,
        processData: false,
        async: false,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Authorization': 'Bearer ' + auth
        },
        data: data,
        success: function(jqXHR) {
            name = jqXHR;
            //console.log(jqXHR);
        },
    });
    return name;
}

function checkStatus(status, value) {
    switch (status) {
        case 1:
            {
                toastr.success(value.name + ' added to Compare', 'Compare');
                // sessionStorage.clear();
                // window.location.href = "compare";
                break;
            };
        case 2:
            {
                toastr.success(value.name + ' added to Cart', 'Cart');
                // sessionStorage.clear();
                // window.location.href = "compare";
                break;
            };
        case 3:
            {
                toastr.success(value.name + ' Cart Quantity Updated', 'Cart');
                // sessionStorage.clear();
                // window.location.href = "compare";
                break;
            };
        case 4:
            {
                toastr.success(value);
                // sessionStorage.clear();
                // window.location.href = "compare";
                break;
            };
        case 5:
            {
                toastr.error(value);
                break;
            };
    }
}

function groupArrayOfObjects(list, key) {
    return list.reduce(function(rv, x) {
        // console.log(rv, x);
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
var map, pos, infoWindow;
var markers = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('maplocation'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 14,
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            console.info(pos);

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    // const apiKey = 'AIzaSyAGBbfja7Mr3UXuJFizf89Q3fQbPY3XYqk';
    // const infoWindow = new google.maps.InfoWindow();

    // Show the information for a store when its marker is clicked.
    // map.data.addListener('click', (event) => {
    //     // const category = event.feature.getProperty('category');
    //     const name = event.feature.getProperty('name');
    //     const description = event.feature.getProperty('description');
    //     const hours = event.feature.getProperty('hours');
    //     const phone = event.feature.getProperty('phone');
    //     const position = event.feature.getGeometry().get();
    //     const content = `
    //   <h2>${name}</h2><p>${description}</p>
    //   <p><b>Open:</b> ${hours}<br/><b>Phone:</b> ${phone}</p>`;

    //     infoWindow.setContent(content);
    //     infoWindow.setPosition(position);
    //     infoWindow.setOptions({ pixelOffset: new google.maps.Size(0, -30) });
    //     infoWindow.open(map);
    // });
}

async function createMarker(branch) {
    console.info(branch)
    var html = "<b>" + branch.name + "</b> <br/>" + branch.address;
    var marker = new google.maps.Marker({
        map: map,
        position: branch.loc
    });

    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });

    markers.push(marker);
}

async function markStore(storeInfo) {
    console.info(storeInfo);
    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: storeInfo.location,
        title: storeInfo.name
    });

    // show store info when marker is clicked
    marker.addListener('click', function() {
        showStoreInfo(storeInfo);
    });
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

// var myStorelocator = new storelocatorjs({
//     options: {
//         apiKey: 'AIzaSyAGBbfja7Mr3UXuJFizf89Q3fQbPY3XYqk'
//     }
// });