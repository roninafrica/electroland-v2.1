var origin = window.location.origin;
var sendEmailPath = origin + "/avnash_signup/sendmail/sendmail.php";

/*
 $(".submit-btn").click(function (event) {
 event.preventDefault();
 send_data(event);
 });
 */

$("#request_form").submit(function (event) {
    event.preventDefault();
  send_data(event);
});

/**
 * 
 * @returns {undefined}
 */
function send_data() {
    var formData = {
        'inputSchoolName': $('input[name=inputSchoolName]').val(),
        'inputSchoolLocation': $('input[name=inputSchoolLocation]').val(),
        'inputContactPerson': $('input[name=inputContactPerson]').val(),
        'inputEmail': $('input[name=inputEmail]').val(),
        'inputPhone': $('input[name=inputPhone]').val(),
        'inputVisitDay': $('input[name=inputVisitDay]').val()
    };

    if (validateTheForm(formData)) {
 $(".submit-btn").attr("disabled", true);
        toastr.warning('Sending details...', {timeOut: 50000});
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: sendEmailPath, // the url where we want to POST
            data: JSON.stringify(formData),
            contentType: 'application/json',
            dataType: 'json' // what type of data do we expect back from the server
        }).done(function (data) {
            toastr.clear();
            toastr.success('Thank you for sending your details.', {timeOut: 5000});
            $('#request_form').trigger('reset');
             $(".submit-btn").attr("disabled", false);

        }).fail(function (data) {
            toastr.clear();
            toastr.error('Request failed, kindly check your network and retry.', {timeOut: 5000});

        });
    }

}



/**
 * 
 * @param {type} $email
 * @returns {Boolean}
 */
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

/**
 * 
 * @param {type} $tel
 * @returns {Boolean}
 */
function validateTel($tel) {
    var filter = /^[0-9-+]+$/;
    return filter.test($tel);
}
/**
 * 
 * @param {type} $num
 * @returns {undefined}
 */
function validateNumeric($num) {
    var filter = /^[1-9]+$/;
    return filter.test($num);
}
/**
 * 
 * @param {type} formData
 * @returns {$valid|Boolean}
 */
function validateTheForm(formData) {
    $valid = true;
    if ($.trim(formData["inputSchoolName"]) === '') {
        toastr.error('Provide school name ', {timeOut: 5000});
        $valid = false;
    }
    if ($.trim(formData["inputSchoolLocation"]) === '') {
        toastr.error('Provide school location ', {timeOut: 5000});
        $valid = false;
    }
    if ($.trim(formData["inputContactPerson"]) === '') {
        toastr.error('Provide contact person ', {timeOut: 5000});
        $valid = false;
    }
    if ($.trim(formData["inputVisitDay"]) === '') {
        toastr.error('Provide your day of visit ', {timeOut: 5000});
        $valid = false;
    }
    if (!validateEmail($.trim(formData["inputEmail"]))) {
        toastr.error('Invalid email provided', {timeOut: 5000});
        $valid = false;
    }
    if (!validateTel($.trim(formData["inputPhone"]))) {
        toastr.error('Invalid phone number', {timeOut: 5000});
        $valid = false;
    }

    return $valid;

}