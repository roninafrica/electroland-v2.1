<?php

Abstract class Routing
{
    private static $routes = array("home","blogs","locations","brands","promotions","about","csr","careers","terms_and_conditions","category","privacy","cookies","corporate","products","support", "compare", "mobile","cart","checkout","contact_us","about_us","search","conditions");

    public static function checkRoute()
    {
        if(empty($_GET['route']))
        {
            $_GET['route'] = "home";
        }
        $_GET['title'] = explode('/', $_GET['route'])[0];
        return (in_array($_GET['title'], self::$routes)) 
                ? $_GET['title'] 
                : "404";
    }
}

?>