<?php

Abstract class products //extends DB_Model
{
    public function processController()
    {
        $url = 'https://electrolandgh.roninafrica.com/api/website/';
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "Products";
                $_GET['description'] = "Home For Quality Electronic";
                $_GET['view'] = "product/products";

                $contents = file_get_contents($url);
                if($contents !== false) { $_GET['brands'] = json_decode($contents, true); }
                
                return array();
                break;
            }
            case 2:   // if url Products/{id}
            {
                $_GET['title'] = "Products";
                $_GET['description'] = "Home For Quality Electronic";
                $_GET['view'] = "product/product";
                // $_GET['product'] = $url.explode('/', $_GET['route'])[1];

                $contents = file_get_contents($url.explode('/', $_GET['route'])[1]);
                if($contents !== false) { 
                    $_GET['product'] = json_decode($contents, true); 
                    $product = $_GET['product'];
                    $_GET['title'] = $product['meta_title'];
                    $_GET['keywords'] = $product['meta_keywords'];
                    $_GET['description'] = $product['meta_description'];
                }
                
                return array();
                break;
            }
            default:
            {
                $_GET['title'] = "404";
                $_GET['description'] = "Page Not Found";
                $_GET['view'] = "404";
                
                return array();
                break;
            }
        }
    }
}

?>