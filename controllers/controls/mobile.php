<?php

Abstract class mobile //extends DB_Model
{
    public function processController()
    {
        $api = 'https://ronin-e-commerce-api.herokuapp.com/api/website/';
        // $capi = 'https://ronin-e-commerce-api.herokuapp.com/api/website/category';
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "home";
                $_GET['description'] = "Home For Quality Electronic";
                $_GET['view'] = "home_mobile";

                $contents = file_get_contents($api.'brands');
                if($contents !== false) { $_GET['brands'] = json_decode($contents, true); }

                $catcontents = file_get_contents($api);
                if($catcontents !== false) { $_GET['category'] = json_decode($catcontents, true); }

                return array();
                break;
            }
            default:
            {
                $_GET['title'] = "404";
                $_GET['description'] = "Page Not Found";
                $_GET['view'] = "404";
                
                return array();
                break;
            }
        }
    }
}

?>