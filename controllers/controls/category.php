<?php

Abstract class category //extends DB_Model
{
    public function processController()
    {
        $url = 'https://electrolandgh.roninafrica.com/api/website/';
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "Category";
                $_GET['description'] = "Home For Quality Electronic";
                $_GET['view'] = "category/category";

                $contents = file_get_contents($url);
                if($contents !== false) { $_GET['brands'] = json_decode($contents, true); }
                
                return array();
                break;
            }
            case 2:   // if url Category/{id}
            {
                $_GET['title'] = "Category";
                $_GET['description'] = "Home For Quality Electronic";
                $_GET['view'] = "category/products";
                // $_GET['page'] = explode('/', $_GET['route'])[1];

                $contents = file_get_contents($url.'category/'.explode('/', $_GET['route'])[1]);
                if($contents !== false) { $_GET['category'] = json_decode($contents, true); }
                
                return array();
                break;
            }
            default:
            {
                $_GET['title'] = "404";
                $_GET['description'] = "Page Not Found";
                $_GET['view'] = "404";
                
                return array();
                break;
            }
        }
    }
}

?>