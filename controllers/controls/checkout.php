<?php

// require_once('../models/DB.php');

Abstract class checkout //extends DB_Model
{
    public function processController()
    {
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Login
            {
                $_GET['title'] = "Checkout";
                $_GET['description'] = "Checkout";
                $_GET['view'] = "checkout/checkout";
                return array();
                break;
            }
            case 3:   // if url checkout/confirmation/{orderid}
            {
                $_GET['title'] = "Checkout";
                $_GET['description'] = "Checkout";
                $_GET['view'] = "checkout/confirm";
                return array();
                break;
            }
            default:
            {
                $_GET['title'] = "404";
                $_GET['description'] = "Page Not Found";
                $_GET['view'] = "404";
                
                return array();
                break;
            }
        }
    }
}
