<?php

Abstract class brands //extends DB_Model
{
    public function processController()
    {
        $api = 'https://electrolandgh.roninafrica.com/api/website/brands/';
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url brands
            {
                $_GET['title'] = "Brands";
                $_GET['description'] = "We Have the Best Brand u Desire";
                $_GET['view'] = "brand/brands";
                // $gapi = $api + $brand;
                $contents = file_get_contents($api.'all');
                if($contents !== false) { 
                    $brand = json_decode($contents, true);
                    $_GET['brands'] = $brand;
                    $_GET['description'] = $brand['description'];
                }
                
                return array();
                break;
            }
            case 2:   // if url brands/{brand}
            {
                $_GET['page'] = "brand";
                switch(explode('/', $_GET['route'])[1])
                {
                    case 'samsung':   // if url brands
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Page";
                        $_GET['view'] = "brand/samsung/samsung";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." See all Available Categories, Promotions, Blog";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'samsung');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }
                        
                        break;
                    }
                    case 'toshiba':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/toshiba/toshiba";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'toshiba');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'nasco':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/nasco/nasco";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'nasco');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'midea':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/midea/midea";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'midea');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'abb':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/abb/abb";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'abb');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'philips':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/philips/philips";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'philips');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'kodak':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/kodak/kodak";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'kodak');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    case 'sollatek':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/sollatek/sollatek";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'sollatek');
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['brand'] = $brand;
                            if(isset($brand)) {
                                if(isset($brand['meta_tag'])) { $_GET['title'] = $brand['meta_tag']; }
                                $_GET['keywords'] = $brand['meta_keywords']; 
                                $_GET['description'] = $brand['meta_description']; 
                                $_GET['image'] = $_GET['imageurl'].$_GET['brand']['banner']; 
                            }
                        }

                        break;
                    }
                    default:
                    {
                        $_GET['title'] = "404";
                        $_GET['description'] = "Page Not Found";
                        $_GET['view'] = "404";
                        
                        return array();
                        break;
                    }
                }

                return array();
                break;
            }
            case 4:   // if url brands/{brand}/category/{id}
            {
                $_GET['title'] = explode('/', $_GET['route'])[1]." Category Product Page";
                $_GET['description'] = "";
                $_GET['view'] = "brand/products";
                $_GET['page'] = explode('/', $_GET['route'])[1];
                
                // $gapi = $api + $brand;
                // $contents = file_get_contents($api.explode('/', $_GET['route'])[1].'/'.explode('/', $_GET['route'])[3]);
                // if($contents !== false) { $_GET['category'] = json_decode($contents, true); }

                switch(explode('/', $_GET['route'])[1])
                {
                    case 'samsung':   // if url brands
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Page";
                        $_GET['view'] = "brand/samsung/products";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." See all Available Categories, Promotions, Blog";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'samsung/'.explode('/', $_GET['route'])[3]);
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['category'] = $brand;
                            $_GET['description'] = $brand['description'];
                        }
                        
                        return array();
                        break;
                    }
                    case 'toshiba':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/toshiba/products";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'toshiba/'.explode('/', $_GET['route'])[3]);
                        // if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['category'] = $brand;
                            $_GET['description'] = $brand['meta_description'];
                        // }

                        return array();
                        break;
                    }
                    case 'nasco':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/nasco/products";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'nasco/'.explode('/', $_GET['route'])[3]);
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['category'] = $brand;
                            $_GET['description'] = $brand['description'];
                        }

                        return array();
                        break;
                    }
                    case 'midea':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/midea/products";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'midea/'.explode('/', $_GET['route'])[3]);
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['category'] = $brand;
                            $_GET['description'] = $brand['description'];
                        }

                        return array();
                        break;
                    }
                    case 'abb':   // if url brands/{brand}
                    {
                        $_GET['title'] = explode('/', $_GET['route'])[1]." Brand Single Page";
                        $_GET['view'] = "brand/abb/products";
                        $_GET['description'] = explode('/', $_GET['route'])[1]." Categories and Products";
                        
                        // $gapi = $api + $brand;
                        $contents = file_get_contents($api.'abb'.explode('/', $_GET['route'])[3]);
                        if($contents !== false) { 
                            $brand = json_decode($contents, true);
                            $_GET['category'] = $brand;
                            $_GET['description'] = $brand['description'];
                        }

                        return array();
                        break;
                    }
                    default:
                    {
                        $_GET['title'] = "404";
                        $_GET['description'] = "Page Not Found";
                        $_GET['view'] = "404";
                        
                        return array();
                        break;
                    }
                }

                // return array();
                break;
            }
            default:
            {
                $_GET['title'] = "404";
                $_GET['description'] = "Page Not Found";
                $_GET['view'] = "404";
                
                return array();
                break;
            }
        }
    }
}

?>