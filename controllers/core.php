<?php

session_start(); 

require_once('../models/lib/routing.php');
// define('DB', '');
define('SITE', 'http://localhost:8888/');
define('IMG', 'http://localhost:4000/public');
define('API', 'https://ronin-e-commerce-api.herokuapp.com/api/website/');

class Controller
{
    private $data, $route;

    public function __construct()
    {
        $this->route = Routing::checkRoute();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->route);
        //unset($this);
    }

    public function Output()
    {
        switch($this->route)
        {
            case "home":
            case "brands":
            case "locations":
            case "category":
            case "promotions":
            case "blogs":
            case "about":
            case "careers":
            case "csr":
            case "products":
            case "privacy":
            case "cookies":
            case "terms_and_conditions":
            case "corporate":
            case "conditions":
            case "compare":
            case "support":
            case "mobile":
            case "cart":
            case "search":
            case "checkout":
            case "contact_us":
            {
                //$_GET['API'] = 'http://localhost:4000/api/website/';
                $this->getModelData(); // Process model via controllel (controls)
                break;
            }
            case "404":
            {
                $_GET['view'] = "404"; 
                break;
            }
            default:
            {
                header('location: home');
                break;
            }
        }
        $this->generateView();  // Generate AND PROCESS VIEW
    }

    private function generateView()
    {
        $route = $_GET['view'];
        $_GET['keywords'] = "Ronin Africa, Electroland, Samsung, Nasco, Toshiba, ABB, Midea";
        $_GET['image'] = "https://demo.roninafrica.com/electroland/assets/img/default_hero.jpg";
        $_GET['imageurl'] = "https://electrolandgh.roninafrica.com/public/";
        if($route == 'home'){
            require_once('../views/components/head.phtml'); // Get VIew
            // require_once('../views/components/nav.phtml'); // Get VIew
            require_once('../views/'.$route.'.phtml'); // Get VIew
            require_once('../views/components/foot.phtml'); // Get VIew
        }else{
            require_once('../views/components/head.phtml'); // Get VIew
            require_once('../views/components/nav.phtml'); // Get VIew
            require_once('../views/'.$route.'.phtml'); // Get VIew
            require_once('../views/components/foot.phtml'); // Get VIew
        }
    }

    private function getModelData()
    {
        $route = $this->route;
        require_once('controls/'.$route.'.php'); // Get COntroller
        $this->data = $route::processController();
    }
}

try
{
    $controller = new Controller();
    $controller->Output();
    unset($controller);
}
catch(Exception $ex)
{
    echo $ex->getMessage();
}
?>